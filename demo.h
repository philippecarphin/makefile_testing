#ifndef _DEMO_ADD_H_
#define _DEMO_ADD_H_
#include <stddef.h>
int demo_add(int a, int b);
int demo_subtract(int a, int b);
size_t demo_filesize(char *filename);
#endif
