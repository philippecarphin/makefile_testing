#include "demo.h"
#include <stdio.h>

int test_demo_subtract_normal(){
	int result = demo_subtract(8,9);
	int expected = -1;
	return result == expected;
}

int test_demo_subtract_big(){
	int result = demo_subtract(100000000,888888888);
	int expected = -788888888;
	return result == expected;
}

int main(void){
	printf("============ Test file ./%s\n", __FILE__);
	if(!test_demo_subtract_normal()) {return 1;}
	if(!test_demo_subtract_big()) {return 1;}
	printf(">>>> SUCCESS : Test file ./%s\n\n", __FILE__);
	return 0;
}
