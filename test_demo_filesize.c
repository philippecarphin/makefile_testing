#include <stdio.h>
#include "demo.h"

int test_use_files(){
    char path[1024];
    snprintf(path, 1023, "%s/%s", TEST_DIR, "small_file.txt");

    size_t result = demo_filesize(path);
    size_t expected = 9;
    return result == expected;
}

int main(void){
	printf("============ Test file ./%s\n", __FILE__);
    printf("%s() TEST_DIR=%s\n", __FUNCTION__, TEST_DIR);
    if(!test_use_files()){ return 1;}
	printf(">>>> SUCCESS : Test file ./%s\n\n", __FILE__);
    return 0;
}
