#include "demo.h"
#include <stdio.h>

int test_demo_add_normal(){
	int result = demo_add(8,9);
	int expected = 17;
	return result == expected;
}

int test_demo_add_big(){
	long long int result = demo_add(100000000,888888888);
	long long int expected = 988888888;
	return result == expected;
}

int main(void){
	printf("============ Test file ./%s\n", __FILE__);
	if(!test_demo_add_normal()) {goto fail;}
	if(!test_demo_add_big()) {goto fail;}
	printf(">>>> SUCCESS : Test file ./%s\n\n", __FILE__);
	return 0;
fail:
	printf(">>>> FAIL : Test file ./%s :(\n\n", __FILE__);
    return 1;
}
