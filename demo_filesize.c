#include <stddef.h>
#include <stdio.h>
#include "demo.h"
size_t demo_filesize(char *filename){
    FILE *fp = fopen(filename, "r");
    fseek(fp, 0L, SEEK_END);
    size_t sz = ftell(fp);
    fclose(fp);
    return sz;
}

